package integration;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleContext;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.tck.junit4.DomainFunctionalTestCase;

public class TestAppITCase extends DomainFunctionalTestCase {

	public static final boolean DEBUG = true;
	private static final String APP_Name = "test-app";
	
	@Override
    protected String getDomainConfig()
    {
        return "mule-domain-config.xml";
    }
	

    @Override
    public ApplicationConfig[] getConfigResources()
    {
    	String configs [] = {"test-app.xml"};
		ApplicationConfig appConfig = new ApplicationConfig(APP_Name, configs); 
		ApplicationConfig[] appConfigs = {appConfig};
		return appConfigs ;        
    }

    @Test
    public void testSharedResource() throws Exception
    {
        //test ok request app
        MuleMessage receivedMessage = getResponseFromApp("http://localhost:8082/test?name=mule", APP_Name);
	    assertEquals("Hi mule", receivedMessage.getPayloadAsString("UTF-8"));
    }

    private MuleMessage getResponseFromApp(String url, String appName) throws MuleException
    {
        MuleContext muleContext = getMuleContextForApp(appName);
        MuleMessage testMessage = new DefaultMuleMessage(null, muleContext);

        return muleContext.getClient().send(url, testMessage);
    }
	
}
