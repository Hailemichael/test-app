package unit;

import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.Date;
import java.util.LinkedHashMap;

import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleContext;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.common.mocking.MessageProcessorMocker;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

public class TestAppUnitTest extends FunctionalMunitSuite {

	public static final boolean DEBUG = false;

	@Override
	protected String getConfigResources() {
		return "test-app.xml";
	}

	// TO Enable Inbound Endpoints
	// @Override protected boolean haveToDisableInboundEndpoints() { return
	// false; }

	// To enable Inbound Endpoints for specific flows

	// @Override protected List<String> getFlowsExcludedOfInboundDisabling() {
	// return Arrays.asList("sqldbconnection.xml","more-config-xml-file.xml"); }

	// To disable Mocking of connectors
	// @Override protected boolean haveToMockMuleConnectors() { return false; }

	// @Override
	protected void muleContextStarted(MuleContext muleContext) {
		System.out.println("Mule Context Started at: " + new Date(muleContext.getStartDate()));
	}

	@Test
	public void testSetPayload() throws Exception {

		// Set input message payload
		Object payload = null;

		// prepare input message properties and set input message
		final MuleMessage muleMessage = new DefaultMuleMessage(payload, muleContext);
		LinkedHashMap<String, String> params = new LinkedHashMap<String, String>();
		params.put("name", "mule");
		muleMessage.setProperty("http.query.params", params, PropertyScope.INBOUND);

		// Create Mule Event that handle the input message
		MuleEvent requestEvent = testEvent(null);
		requestEvent.setMessage(muleMessage);
		MuleEvent event = runFlow("test-appFlow", requestEvent);

		// Get the actual Employee data from the received message
		MuleMessage receivedMessage = event.getMessage();
		if (DEBUG)
			System.out.println("Recieved Message: " + receivedMessage);

		assertEquals("Hi mule", receivedMessage.getPayloadAsString("UTF-8"));
	}

}
